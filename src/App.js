import React, { Component } from "react";
import "./App.css";
class App extends Component {
  state = {
    name: "Binod Nagarkoti",
    address: "Boudha",
    phoneNumber: "98989898989"
  };

  render() {
    return (
      <div className="App">
        <h1> First React App </h1>
        Name :{this.state.name}
        <br />
        Addres :{this.state.address}
        <br />
        Phone Number: {this.state.phoneNumber}
      </div>
    );
  }
}

export default App;
